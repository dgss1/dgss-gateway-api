package com.dgss.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import brave.sampler.Sampler;

@EnableDiscoveryClient
@SpringBootApplication
@EnableZuulProxy
@ComponentScan(basePackages="com.dgss.gateway")
public class DgssGatewayApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DgssGatewayApiApplication.class, args);

	}
	
	@Bean
	public Sampler defautlSamler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}
